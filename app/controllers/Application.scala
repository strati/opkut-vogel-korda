package controllers

import models._
import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index(matrixDimensionsForm))
  }

  def validateDimensions = Action { implicit request =>
    matrixDimensionsForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest(views.html.index(formWithErrors))
      },
      (dimensions: MatrixDimensions) => {
        /* binding success, you get the actual value. */
        Redirect(routes.Application.displayInputTable(dimensions.senderCount, dimensions.receiverCount))
      }
    )
  }

  def displayInputTable(senderCount: Int, receiverCount: Int) = Action { implicit request =>
    val matrixDimensions: MatrixDimensions = MatrixDimensions(senderCount, receiverCount)
    Ok(
      views.html.displayInputTable(
        tableForm.fill(DataTable.empty),
        matrixDimensions)
    )
  }

  def calculate(senderCount: Int, receiverCount: Int) = Action { implicit request =>
    tableForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.displayInputTable(formWithErrors, MatrixDimensions(senderCount, receiverCount)))
      },
      (matrix: DataTable) => {
        val solution = getSolution(matrix.reduceCosts)
        Ok(views.html.solution(matrix, solution, MatrixDimensions(senderCount, receiverCount)))
      }
    )
  }

  private def getSolution(m: DataTable, acc: List[DataTable] = List.empty[DataTable]): List[DataTable] = {
    if (m.isCompleted) {
      acc
    } else {
      val newDataTable = m.transferMostOptimal
      getSolution(newDataTable, acc :+ newDataTable)
    }
  }

}