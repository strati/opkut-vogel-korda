import play.api.data.Form
import play.api.data.Forms._

package object models {

    val matrixDimensionsForm = Form(
      mapping(
        "senderCount" -> number(min = 2, max = 10),
        "receiverCount" -> number(min = 2, max = 10)
      )(MatrixDimensions.apply)(MatrixDimensions.unapply)
    )

    val tableForm = Form(
      mapping(
        "cost" -> seq(seq(default(number(min = 0, max = 500), 0))),
        "stock" -> seq(default(number(min = 0, max = 500), 0)),
        "need" -> seq(default(number(min = 0, max = 500), 0)),
        "transfers" -> ignored(List.empty[Transfer])
      )(DataTable.apply)(DataTable.unapply)
      verifying(
        "Az igények és a készletek összege nem megegyező!", (fields: DataTable) => fields match {
        case matrix: DataTable => matrix.isStocksMatchNeeds
      })
    )
}
