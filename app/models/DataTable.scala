package models

import java.lang.Math.min

case class Transfer(from: Int, to: Int, amount: Int)

object DataTable {

  def empty: DataTable = {
    DataTable(
      Seq.empty,
      Seq.empty,
      Seq.empty,
      List.empty
    )
  }
}

case class DataTable(costs: Seq[Seq[Int]], stocks: Seq[Int], needs: Seq[Int], transfers: List[Transfer]) {

  lazy val senderCount: Int = stocks.length

  lazy val receiverCount: Int = needs.length

  lazy val senderDifferences: Seq[Int] = {
    costs.map(getDifferenceOfMinimumCosts(isColumnEmpty))
  }

  lazy val receiverDifferences: Seq[Int] = {
    costsByReceiver.map(getDifferenceOfMinimumCosts(isRowEmpty))
  }

  def isRowEmpty(i: Int): Boolean = stocks(i) == 0

  def isColumnEmpty(i: Int): Boolean = needs(i) == 0

  def isStocksMatchNeeds: Boolean = {
    stocks.sum == needs.sum
  }

  def isCompleted: Boolean = {
    !needs.exists(_ > 0)
  }

  def transferMostOptimal: DataTable = {
    if (needs.count(_ > 0) == 1 && stocks.count(_ > 0) == 1) {
      val rowIndex = stocks.indexWhere(_ > 0)
      val columnIndex = needs.indexWhere(_ > 0)
      val transferredAmount = needs(columnIndex)

      val transfer = Transfer(rowIndex, columnIndex, transferredAmount)

      DataTable(
        costs.updated(rowIndex, Seq.fill(receiverCount)(Int.MaxValue)),
        stocks.updated(rowIndex, stocks(rowIndex) - transferredAmount),
        needs.updated(columnIndex, needs(columnIndex) - transferredAmount),
        transfers :+ transfer
      )
    } else {
      val senderMax = senderDifferences.max
      val receiverMax = receiverDifferences.max

      if (senderMax > receiverMax) {
        val rowIndex = senderDifferences.indexOf(senderMax)
        val minimumOfRow = costs(rowIndex).min
        val columnIndex = costs(rowIndex).indexOf(minimumOfRow)
        val needsAtColumn = needs(columnIndex)
        val stocksAtRow = stocks(rowIndex)
        val transferredAmount = min(needsAtColumn, stocksAtRow)

        val transfer = Transfer(rowIndex, columnIndex, transferredAmount)

        DataTable(
          costs.updated(rowIndex, Seq.fill(receiverCount)(Int.MaxValue)),
          stocks.updated(rowIndex, stocks(rowIndex) - transferredAmount),
          needs.updated(columnIndex, needs(columnIndex) - transferredAmount),
          transfers :+ transfer
        )
      } else {
        val rowIndex = receiverDifferences.indexOf(receiverMax)
        val minimumOfRow = costsByReceiver(rowIndex).min
        val columnIndex = costsByReceiver(rowIndex).indexOf(minimumOfRow)
        val needsAtColumn = needs(rowIndex)
        val stocksAtRow = stocks(columnIndex)
        val transferredAmount = min(needsAtColumn, stocksAtRow)

        val transfer = Transfer(columnIndex, rowIndex, transferredAmount)

        DataTable(
          costs.map(_.updated(rowIndex, Int.MaxValue)),
          stocks.updated(columnIndex, stocks(columnIndex) - transferredAmount),
          needs.updated(rowIndex, needs(rowIndex) - transferredAmount),
          transfers :+ transfer
        )
      }
    }
  }

  /**
   * árak redukálása:
   * egy sor mindegyik eleméből levonjuk a sor legkisebb elemét
   */
  def reduceCosts: DataTable = {
    val reducedCosts = costs.map { (rowCosts: Seq[Int]) =>
      val minimumPrice = rowCosts.min
      rowCosts.map(_ - minimumPrice)
    }
    DataTable(reducedCosts, stocks, needs, transfers)
  }

  private def getDifferenceOfMinimumCosts(rowEmptyChecker: Int => Boolean)(costs: Seq[Int]): Int = {
    val filteredRowCosts: Seq[Int] = costs.zipWithIndex.foldLeft(Seq.empty[Int]) { case (acc, costWithIndex) =>
      if (rowEmptyChecker(costWithIndex._2)) {
        acc
      } else {
        acc :+ costWithIndex._1
      }
    }
    val minimums = filteredRowCosts.sorted.take(2)
    if (minimums.length == 2) {
      minimums(1) - minimums(0)
    } else {
      0
    }
  }

  lazy val costsByReceiver: Seq[Seq[Int]] = {
    for {
      i <- 0 to receiverCount - 1
    } yield {
      for {
        j <- 0 to senderCount - 1
      } yield {
        costs(j)(i)
      }
    }
  }
}