package models

case class MatrixDimensions(senderCount: Int, receiverCount: Int)
